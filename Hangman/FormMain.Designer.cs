﻿namespace Hangman
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.tabControlMain = new System.Windows.Forms.TabControl();
            this.tabPageGame = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBoxStatus = new System.Windows.Forms.GroupBox();
            this.pictureBoxActualColorSetting = new System.Windows.Forms.PictureBox();
            this.labelNameSetting = new System.Windows.Forms.Label();
            this.labelColor = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.groupBoxLetters = new System.Windows.Forms.GroupBox();
            this.labelZ = new System.Windows.Forms.Label();
            this.labelY = new System.Windows.Forms.Label();
            this.labelX = new System.Windows.Forms.Label();
            this.labelW = new System.Windows.Forms.Label();
            this.labelV = new System.Windows.Forms.Label();
            this.labelU = new System.Windows.Forms.Label();
            this.labelT = new System.Windows.Forms.Label();
            this.labelS = new System.Windows.Forms.Label();
            this.labelR = new System.Windows.Forms.Label();
            this.labelQ = new System.Windows.Forms.Label();
            this.labelP = new System.Windows.Forms.Label();
            this.labelO = new System.Windows.Forms.Label();
            this.labelN = new System.Windows.Forms.Label();
            this.labelM = new System.Windows.Forms.Label();
            this.labelL = new System.Windows.Forms.Label();
            this.labelK = new System.Windows.Forms.Label();
            this.labelJ = new System.Windows.Forms.Label();
            this.labelI = new System.Windows.Forms.Label();
            this.labelH = new System.Windows.Forms.Label();
            this.labelG = new System.Windows.Forms.Label();
            this.labelF = new System.Windows.Forms.Label();
            this.labelE = new System.Windows.Forms.Label();
            this.labelD = new System.Windows.Forms.Label();
            this.labelC = new System.Windows.Forms.Label();
            this.labelB = new System.Windows.Forms.Label();
            this.labelA = new System.Windows.Forms.Label();
            this.labelActualWord = new System.Windows.Forms.Label();
            this.groupBoxWord = new System.Windows.Forms.GroupBox();
            this.labelWord = new System.Windows.Forms.Label();
            this.groupBoxControls = new System.Windows.Forms.GroupBox();
            this.buttonClose = new System.Windows.Forms.Button();
            this.buttonNewGame = new System.Windows.Forms.Button();
            this.groupBoxHang = new System.Windows.Forms.GroupBox();
            this.pictureBoxImage = new System.Windows.Forms.PictureBox();
            this.tabPageSettings = new System.Windows.Forms.TabPage();
            this.groupBoxName = new System.Windows.Forms.GroupBox();
            this.buttonNameSave = new System.Windows.Forms.Button();
            this.textBoxNameChange = new System.Windows.Forms.TextBox();
            this.groupBoxColor = new System.Windows.Forms.GroupBox();
            this.pictureBoxActualColor = new System.Windows.Forms.PictureBox();
            this.labelActualColor = new System.Windows.Forms.Label();
            this.groupBoxWords = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxAllWords = new System.Windows.Forms.TextBox();
            this.tabPageAbout = new System.Windows.Forms.TabPage();
            this.textBoxAbout = new System.Windows.Forms.TextBox();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.labelWorldSolve = new System.Windows.Forms.Label();
            this.tabControlMain.SuspendLayout();
            this.tabPageGame.SuspendLayout();
            this.groupBoxStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxActualColorSetting)).BeginInit();
            this.groupBoxLetters.SuspendLayout();
            this.groupBoxWord.SuspendLayout();
            this.groupBoxControls.SuspendLayout();
            this.groupBoxHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxImage)).BeginInit();
            this.tabPageSettings.SuspendLayout();
            this.groupBoxName.SuspendLayout();
            this.groupBoxColor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxActualColor)).BeginInit();
            this.groupBoxWords.SuspendLayout();
            this.tabPageAbout.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControlMain
            // 
            this.tabControlMain.Controls.Add(this.tabPageGame);
            this.tabControlMain.Controls.Add(this.tabPageSettings);
            this.tabControlMain.Controls.Add(this.tabPageAbout);
            this.tabControlMain.Location = new System.Drawing.Point(12, 12);
            this.tabControlMain.Name = "tabControlMain";
            this.tabControlMain.SelectedIndex = 0;
            this.tabControlMain.Size = new System.Drawing.Size(571, 392);
            this.tabControlMain.TabIndex = 0;
            this.tabControlMain.SelectedIndexChanged += new System.EventHandler(this.tabControlMain_SelectedIndexChanged);
            // 
            // tabPageGame
            // 
            this.tabPageGame.BackColor = System.Drawing.SystemColors.Control;
            this.tabPageGame.Controls.Add(this.label1);
            this.tabPageGame.Controls.Add(this.groupBoxStatus);
            this.tabPageGame.Controls.Add(this.progressBar);
            this.tabPageGame.Controls.Add(this.groupBoxLetters);
            this.tabPageGame.Controls.Add(this.groupBoxWord);
            this.tabPageGame.Controls.Add(this.groupBoxControls);
            this.tabPageGame.Controls.Add(this.groupBoxHang);
            this.tabPageGame.Location = new System.Drawing.Point(4, 22);
            this.tabPageGame.Name = "tabPageGame";
            this.tabPageGame.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageGame.Size = new System.Drawing.Size(563, 366);
            this.tabPageGame.TabIndex = 0;
            this.tabPageGame.Text = "Game";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(354, 93);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Lebenssaft";
            // 
            // groupBoxStatus
            // 
            this.groupBoxStatus.Controls.Add(this.pictureBoxActualColorSetting);
            this.groupBoxStatus.Controls.Add(this.labelNameSetting);
            this.groupBoxStatus.Controls.Add(this.labelColor);
            this.groupBoxStatus.Controls.Add(this.labelName);
            this.groupBoxStatus.Location = new System.Drawing.Point(354, 152);
            this.groupBoxStatus.Name = "groupBoxStatus";
            this.groupBoxStatus.Size = new System.Drawing.Size(192, 82);
            this.groupBoxStatus.TabIndex = 6;
            this.groupBoxStatus.TabStop = false;
            this.groupBoxStatus.Text = "Status";
            // 
            // pictureBoxActualColorSetting
            // 
            this.pictureBoxActualColorSetting.BackColor = System.Drawing.Color.Black;
            this.pictureBoxActualColorSetting.Location = new System.Drawing.Point(137, 58);
            this.pictureBoxActualColorSetting.Name = "pictureBoxActualColorSetting";
            this.pictureBoxActualColorSetting.Size = new System.Drawing.Size(33, 13);
            this.pictureBoxActualColorSetting.TabIndex = 5;
            this.pictureBoxActualColorSetting.TabStop = false;
            // 
            // labelNameSetting
            // 
            this.labelNameSetting.AutoSize = true;
            this.labelNameSetting.Location = new System.Drawing.Point(134, 31);
            this.labelNameSetting.Name = "labelNameSetting";
            this.labelNameSetting.Size = new System.Drawing.Size(36, 13);
            this.labelNameSetting.TabIndex = 3;
            this.labelNameSetting.Text = "Player";
            // 
            // labelColor
            // 
            this.labelColor.AutoSize = true;
            this.labelColor.Location = new System.Drawing.Point(6, 58);
            this.labelColor.Name = "labelColor";
            this.labelColor.Size = new System.Drawing.Size(34, 13);
            this.labelColor.TabIndex = 2;
            this.labelColor.Text = "Color:";
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(6, 31);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(38, 13);
            this.labelName.TabIndex = 0;
            this.labelName.Text = "Name:";
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(354, 103);
            this.progressBar.Maximum = 6;
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(186, 23);
            this.progressBar.Step = 6;
            this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar.TabIndex = 5;
            this.progressBar.Value = 6;
            // 
            // groupBoxLetters
            // 
            this.groupBoxLetters.Controls.Add(this.labelZ);
            this.groupBoxLetters.Controls.Add(this.labelY);
            this.groupBoxLetters.Controls.Add(this.labelX);
            this.groupBoxLetters.Controls.Add(this.labelW);
            this.groupBoxLetters.Controls.Add(this.labelV);
            this.groupBoxLetters.Controls.Add(this.labelU);
            this.groupBoxLetters.Controls.Add(this.labelT);
            this.groupBoxLetters.Controls.Add(this.labelS);
            this.groupBoxLetters.Controls.Add(this.labelR);
            this.groupBoxLetters.Controls.Add(this.labelQ);
            this.groupBoxLetters.Controls.Add(this.labelP);
            this.groupBoxLetters.Controls.Add(this.labelO);
            this.groupBoxLetters.Controls.Add(this.labelN);
            this.groupBoxLetters.Controls.Add(this.labelM);
            this.groupBoxLetters.Controls.Add(this.labelL);
            this.groupBoxLetters.Controls.Add(this.labelK);
            this.groupBoxLetters.Controls.Add(this.labelJ);
            this.groupBoxLetters.Controls.Add(this.labelI);
            this.groupBoxLetters.Controls.Add(this.labelH);
            this.groupBoxLetters.Controls.Add(this.labelG);
            this.groupBoxLetters.Controls.Add(this.labelF);
            this.groupBoxLetters.Controls.Add(this.labelE);
            this.groupBoxLetters.Controls.Add(this.labelD);
            this.groupBoxLetters.Controls.Add(this.labelC);
            this.groupBoxLetters.Controls.Add(this.labelB);
            this.groupBoxLetters.Controls.Add(this.labelA);
            this.groupBoxLetters.Controls.Add(this.labelActualWord);
            this.groupBoxLetters.Location = new System.Drawing.Point(7, 49);
            this.groupBoxLetters.Name = "groupBoxLetters";
            this.groupBoxLetters.Size = new System.Drawing.Size(539, 37);
            this.groupBoxLetters.TabIndex = 4;
            this.groupBoxLetters.TabStop = false;
            this.groupBoxLetters.Text = "Letters";
            // 
            // labelZ
            // 
            this.labelZ.AutoSize = true;
            this.labelZ.Location = new System.Drawing.Point(481, 16);
            this.labelZ.Name = "labelZ";
            this.labelZ.Size = new System.Drawing.Size(12, 13);
            this.labelZ.TabIndex = 27;
            this.labelZ.Text = "z";
            this.labelZ.Click += new System.EventHandler(this.labelZ_Click);
            // 
            // labelY
            // 
            this.labelY.AutoSize = true;
            this.labelY.Location = new System.Drawing.Point(462, 16);
            this.labelY.Name = "labelY";
            this.labelY.Size = new System.Drawing.Size(12, 13);
            this.labelY.TabIndex = 26;
            this.labelY.Text = "y";
            this.labelY.Click += new System.EventHandler(this.labelY_Click);
            // 
            // labelX
            // 
            this.labelX.AutoSize = true;
            this.labelX.Location = new System.Drawing.Point(443, 16);
            this.labelX.Name = "labelX";
            this.labelX.Size = new System.Drawing.Size(12, 13);
            this.labelX.TabIndex = 25;
            this.labelX.Text = "x";
            this.labelX.Click += new System.EventHandler(this.labelX_Click);
            // 
            // labelW
            // 
            this.labelW.AutoSize = true;
            this.labelW.Location = new System.Drawing.Point(424, 16);
            this.labelW.Name = "labelW";
            this.labelW.Size = new System.Drawing.Size(15, 13);
            this.labelW.TabIndex = 24;
            this.labelW.Text = "w";
            this.labelW.Click += new System.EventHandler(this.labelW_Click);
            // 
            // labelV
            // 
            this.labelV.AutoSize = true;
            this.labelV.Location = new System.Drawing.Point(405, 16);
            this.labelV.Name = "labelV";
            this.labelV.Size = new System.Drawing.Size(13, 13);
            this.labelV.TabIndex = 23;
            this.labelV.Text = "v";
            this.labelV.Click += new System.EventHandler(this.labelV_Click);
            // 
            // labelU
            // 
            this.labelU.AutoSize = true;
            this.labelU.Location = new System.Drawing.Point(385, 16);
            this.labelU.Name = "labelU";
            this.labelU.Size = new System.Drawing.Size(13, 13);
            this.labelU.TabIndex = 22;
            this.labelU.Text = "u";
            this.labelU.Click += new System.EventHandler(this.labelU_Click);
            // 
            // labelT
            // 
            this.labelT.AutoSize = true;
            this.labelT.Location = new System.Drawing.Point(366, 16);
            this.labelT.Name = "labelT";
            this.labelT.Size = new System.Drawing.Size(10, 13);
            this.labelT.TabIndex = 21;
            this.labelT.Text = "t";
            this.labelT.Click += new System.EventHandler(this.labelT_Click);
            // 
            // labelS
            // 
            this.labelS.AutoSize = true;
            this.labelS.Location = new System.Drawing.Point(347, 16);
            this.labelS.Name = "labelS";
            this.labelS.Size = new System.Drawing.Size(12, 13);
            this.labelS.TabIndex = 20;
            this.labelS.Text = "s";
            this.labelS.Click += new System.EventHandler(this.labelS_Click);
            // 
            // labelR
            // 
            this.labelR.AutoSize = true;
            this.labelR.Location = new System.Drawing.Point(333, 16);
            this.labelR.Name = "labelR";
            this.labelR.Size = new System.Drawing.Size(10, 13);
            this.labelR.TabIndex = 19;
            this.labelR.Text = "r";
            this.labelR.Click += new System.EventHandler(this.labelR_Click);
            // 
            // labelQ
            // 
            this.labelQ.AutoSize = true;
            this.labelQ.Location = new System.Drawing.Point(314, 16);
            this.labelQ.Name = "labelQ";
            this.labelQ.Size = new System.Drawing.Size(13, 13);
            this.labelQ.TabIndex = 18;
            this.labelQ.Text = "q";
            this.labelQ.Click += new System.EventHandler(this.labelQ_Click);
            // 
            // labelP
            // 
            this.labelP.AutoSize = true;
            this.labelP.Location = new System.Drawing.Point(295, 16);
            this.labelP.Name = "labelP";
            this.labelP.Size = new System.Drawing.Size(13, 13);
            this.labelP.TabIndex = 17;
            this.labelP.Text = "p";
            this.labelP.Click += new System.EventHandler(this.labelP_Click);
            // 
            // labelO
            // 
            this.labelO.AutoSize = true;
            this.labelO.Location = new System.Drawing.Point(279, 16);
            this.labelO.Name = "labelO";
            this.labelO.Size = new System.Drawing.Size(13, 13);
            this.labelO.TabIndex = 16;
            this.labelO.Text = "o";
            this.labelO.Click += new System.EventHandler(this.labelO_Click);
            // 
            // labelN
            // 
            this.labelN.AutoSize = true;
            this.labelN.Location = new System.Drawing.Point(260, 16);
            this.labelN.Name = "labelN";
            this.labelN.Size = new System.Drawing.Size(13, 13);
            this.labelN.TabIndex = 15;
            this.labelN.Text = "n";
            this.labelN.Click += new System.EventHandler(this.labelN_Click);
            // 
            // labelM
            // 
            this.labelM.AutoSize = true;
            this.labelM.Location = new System.Drawing.Point(241, 16);
            this.labelM.Name = "labelM";
            this.labelM.Size = new System.Drawing.Size(15, 13);
            this.labelM.TabIndex = 14;
            this.labelM.Text = "m";
            this.labelM.Click += new System.EventHandler(this.labelM_Click);
            // 
            // labelL
            // 
            this.labelL.AutoSize = true;
            this.labelL.Location = new System.Drawing.Point(219, 16);
            this.labelL.Name = "labelL";
            this.labelL.Size = new System.Drawing.Size(9, 13);
            this.labelL.TabIndex = 13;
            this.labelL.Text = "l";
            this.labelL.Click += new System.EventHandler(this.labelL_Click);
            // 
            // labelK
            // 
            this.labelK.AutoSize = true;
            this.labelK.Location = new System.Drawing.Point(200, 16);
            this.labelK.Name = "labelK";
            this.labelK.Size = new System.Drawing.Size(13, 13);
            this.labelK.TabIndex = 12;
            this.labelK.Text = "k";
            this.labelK.Click += new System.EventHandler(this.labelK_Click);
            // 
            // labelJ
            // 
            this.labelJ.AutoSize = true;
            this.labelJ.Location = new System.Drawing.Point(181, 16);
            this.labelJ.Name = "labelJ";
            this.labelJ.Size = new System.Drawing.Size(9, 13);
            this.labelJ.TabIndex = 11;
            this.labelJ.Text = "j";
            this.labelJ.Click += new System.EventHandler(this.labelJ_Click);
            // 
            // labelI
            // 
            this.labelI.AutoSize = true;
            this.labelI.Location = new System.Drawing.Point(161, 16);
            this.labelI.Name = "labelI";
            this.labelI.Size = new System.Drawing.Size(9, 13);
            this.labelI.TabIndex = 10;
            this.labelI.Text = "i";
            this.labelI.Click += new System.EventHandler(this.labelI_Click);
            // 
            // labelH
            // 
            this.labelH.AutoSize = true;
            this.labelH.Location = new System.Drawing.Point(142, 16);
            this.labelH.Name = "labelH";
            this.labelH.Size = new System.Drawing.Size(13, 13);
            this.labelH.TabIndex = 9;
            this.labelH.Text = "h";
            this.labelH.Click += new System.EventHandler(this.labelH_Click);
            // 
            // labelG
            // 
            this.labelG.AutoSize = true;
            this.labelG.Location = new System.Drawing.Point(123, 16);
            this.labelG.Name = "labelG";
            this.labelG.Size = new System.Drawing.Size(13, 13);
            this.labelG.TabIndex = 8;
            this.labelG.Text = "g";
            this.labelG.Click += new System.EventHandler(this.labelG_Click);
            // 
            // labelF
            // 
            this.labelF.AutoSize = true;
            this.labelF.Location = new System.Drawing.Point(103, 16);
            this.labelF.Name = "labelF";
            this.labelF.Size = new System.Drawing.Size(10, 13);
            this.labelF.TabIndex = 7;
            this.labelF.Text = "f";
            this.labelF.Click += new System.EventHandler(this.labelF_Click);
            // 
            // labelE
            // 
            this.labelE.AutoSize = true;
            this.labelE.Location = new System.Drawing.Point(84, 16);
            this.labelE.Name = "labelE";
            this.labelE.Size = new System.Drawing.Size(13, 13);
            this.labelE.TabIndex = 6;
            this.labelE.Text = "e";
            this.labelE.Click += new System.EventHandler(this.labelE_Click);
            // 
            // labelD
            // 
            this.labelD.AutoSize = true;
            this.labelD.Location = new System.Drawing.Point(65, 16);
            this.labelD.Name = "labelD";
            this.labelD.Size = new System.Drawing.Size(13, 13);
            this.labelD.TabIndex = 5;
            this.labelD.Text = "d";
            this.labelD.Click += new System.EventHandler(this.labelD_Click);
            // 
            // labelC
            // 
            this.labelC.AutoSize = true;
            this.labelC.Location = new System.Drawing.Point(48, 16);
            this.labelC.Name = "labelC";
            this.labelC.Size = new System.Drawing.Size(13, 13);
            this.labelC.TabIndex = 4;
            this.labelC.Text = "c";
            this.labelC.Click += new System.EventHandler(this.labelC_Click);
            // 
            // labelB
            // 
            this.labelB.AutoSize = true;
            this.labelB.Location = new System.Drawing.Point(28, 16);
            this.labelB.Name = "labelB";
            this.labelB.Size = new System.Drawing.Size(13, 13);
            this.labelB.TabIndex = 3;
            this.labelB.Text = "b";
            this.labelB.Click += new System.EventHandler(this.labelB_Click);
            // 
            // labelA
            // 
            this.labelA.AutoSize = true;
            this.labelA.Location = new System.Drawing.Point(9, 16);
            this.labelA.Name = "labelA";
            this.labelA.Size = new System.Drawing.Size(13, 13);
            this.labelA.TabIndex = 2;
            this.labelA.Text = "a";
            this.labelA.Click += new System.EventHandler(this.labelA_Click);
            // 
            // labelActualWord
            // 
            this.labelActualWord.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelActualWord.Location = new System.Drawing.Point(6, 11);
            this.labelActualWord.Name = "labelActualWord";
            this.labelActualWord.Size = new System.Drawing.Size(527, 23);
            this.labelActualWord.TabIndex = 1;
            this.labelActualWord.Text = "\r\n";
            this.labelActualWord.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBoxWord
            // 
            this.groupBoxWord.Controls.Add(this.labelWord);
            this.groupBoxWord.Location = new System.Drawing.Point(7, 6);
            this.groupBoxWord.Name = "groupBoxWord";
            this.groupBoxWord.Size = new System.Drawing.Size(539, 37);
            this.groupBoxWord.TabIndex = 3;
            this.groupBoxWord.TabStop = false;
            this.groupBoxWord.Text = "Word";
            // 
            // labelWord
            // 
            this.labelWord.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelWord.Location = new System.Drawing.Point(6, 11);
            this.labelWord.Name = "labelWord";
            this.labelWord.Size = new System.Drawing.Size(527, 23);
            this.labelWord.TabIndex = 0;
            this.labelWord.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBoxControls
            // 
            this.groupBoxControls.Controls.Add(this.buttonClose);
            this.groupBoxControls.Controls.Add(this.buttonNewGame);
            this.groupBoxControls.Location = new System.Drawing.Point(354, 257);
            this.groupBoxControls.Name = "groupBoxControls";
            this.groupBoxControls.Size = new System.Drawing.Size(192, 95);
            this.groupBoxControls.TabIndex = 2;
            this.groupBoxControls.TabStop = false;
            this.groupBoxControls.Text = "Controls";
            // 
            // buttonClose
            // 
            this.buttonClose.Location = new System.Drawing.Point(6, 52);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(180, 32);
            this.buttonClose.TabIndex = 4;
            this.buttonClose.Text = "Close";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // buttonNewGame
            // 
            this.buttonNewGame.Location = new System.Drawing.Point(6, 19);
            this.buttonNewGame.Name = "buttonNewGame";
            this.buttonNewGame.Size = new System.Drawing.Size(180, 27);
            this.buttonNewGame.TabIndex = 1;
            this.buttonNewGame.Text = "New Game";
            this.buttonNewGame.UseVisualStyleBackColor = true;
            this.buttonNewGame.Click += new System.EventHandler(this.buttonNewGame_Click);
            // 
            // groupBoxHang
            // 
            this.groupBoxHang.Controls.Add(this.pictureBoxImage);
            this.groupBoxHang.Location = new System.Drawing.Point(7, 92);
            this.groupBoxHang.Name = "groupBoxHang";
            this.groupBoxHang.Size = new System.Drawing.Size(282, 260);
            this.groupBoxHang.TabIndex = 0;
            this.groupBoxHang.TabStop = false;
            this.groupBoxHang.Text = "strTextGroupBox";
            // 
            // pictureBoxImage
            // 
            this.pictureBoxImage.Location = new System.Drawing.Point(6, 19);
            this.pictureBoxImage.Name = "pictureBoxImage";
            this.pictureBoxImage.Size = new System.Drawing.Size(267, 235);
            this.pictureBoxImage.TabIndex = 0;
            this.pictureBoxImage.TabStop = false;
            // 
            // tabPageSettings
            // 
            this.tabPageSettings.BackColor = System.Drawing.SystemColors.Control;
            this.tabPageSettings.Controls.Add(this.groupBoxName);
            this.tabPageSettings.Controls.Add(this.groupBoxColor);
            this.tabPageSettings.Controls.Add(this.groupBoxWords);
            this.tabPageSettings.Location = new System.Drawing.Point(4, 22);
            this.tabPageSettings.Name = "tabPageSettings";
            this.tabPageSettings.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSettings.Size = new System.Drawing.Size(563, 366);
            this.tabPageSettings.TabIndex = 1;
            this.tabPageSettings.Text = "Settings";
            // 
            // groupBoxName
            // 
            this.groupBoxName.Controls.Add(this.buttonNameSave);
            this.groupBoxName.Controls.Add(this.textBoxNameChange);
            this.groupBoxName.Location = new System.Drawing.Point(6, 212);
            this.groupBoxName.Name = "groupBoxName";
            this.groupBoxName.Size = new System.Drawing.Size(551, 59);
            this.groupBoxName.TabIndex = 2;
            this.groupBoxName.TabStop = false;
            this.groupBoxName.Text = "Name";
            // 
            // buttonNameSave
            // 
            this.buttonNameSave.Location = new System.Drawing.Point(283, 19);
            this.buttonNameSave.Name = "buttonNameSave";
            this.buttonNameSave.Size = new System.Drawing.Size(262, 19);
            this.buttonNameSave.TabIndex = 1;
            this.buttonNameSave.Text = "Save";
            this.buttonNameSave.UseVisualStyleBackColor = true;
            this.buttonNameSave.Click += new System.EventHandler(this.buttonNameSave_Click);
            // 
            // textBoxNameChange
            // 
            this.textBoxNameChange.Location = new System.Drawing.Point(7, 19);
            this.textBoxNameChange.Name = "textBoxNameChange";
            this.textBoxNameChange.Size = new System.Drawing.Size(269, 20);
            this.textBoxNameChange.TabIndex = 0;
            // 
            // groupBoxColor
            // 
            this.groupBoxColor.Controls.Add(this.pictureBoxActualColor);
            this.groupBoxColor.Controls.Add(this.labelActualColor);
            this.groupBoxColor.Location = new System.Drawing.Point(6, 277);
            this.groupBoxColor.Name = "groupBoxColor";
            this.groupBoxColor.Size = new System.Drawing.Size(551, 59);
            this.groupBoxColor.TabIndex = 2;
            this.groupBoxColor.TabStop = false;
            this.groupBoxColor.Text = "Color";
            // 
            // pictureBoxActualColor
            // 
            this.pictureBoxActualColor.BackColor = System.Drawing.Color.Black;
            this.pictureBoxActualColor.Location = new System.Drawing.Point(283, 25);
            this.pictureBoxActualColor.Name = "pictureBoxActualColor";
            this.pictureBoxActualColor.Size = new System.Drawing.Size(262, 13);
            this.pictureBoxActualColor.TabIndex = 1;
            this.pictureBoxActualColor.TabStop = false;
            this.pictureBoxActualColor.Click += new System.EventHandler(this.pictureBoxActualColor_Click);
            // 
            // labelActualColor
            // 
            this.labelActualColor.AutoSize = true;
            this.labelActualColor.Location = new System.Drawing.Point(27, 25);
            this.labelActualColor.Name = "labelActualColor";
            this.labelActualColor.Size = new System.Drawing.Size(64, 13);
            this.labelActualColor.TabIndex = 0;
            this.labelActualColor.Text = "Actual Color";
            // 
            // groupBoxWords
            // 
            this.groupBoxWords.Controls.Add(this.label2);
            this.groupBoxWords.Controls.Add(this.textBoxAllWords);
            this.groupBoxWords.Location = new System.Drawing.Point(6, 6);
            this.groupBoxWords.Name = "groupBoxWords";
            this.groupBoxWords.Size = new System.Drawing.Size(551, 200);
            this.groupBoxWords.TabIndex = 1;
            this.groupBoxWords.TabStop = false;
            this.groupBoxWords.Text = "Words";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(6, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(533, 26);
            this.label2.TabIndex = 1;
            this.label2.Text = "Bitte Wörter ohne Zahlen und Sonerzeichen auswählen, Wörter sollten kürzer als 13" +
    " Zeichen sein sonst werden\r\nsie nicht genommen";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxAllWords
            // 
            this.textBoxAllWords.Location = new System.Drawing.Point(6, 45);
            this.textBoxAllWords.Multiline = true;
            this.textBoxAllWords.Name = "textBoxAllWords";
            this.textBoxAllWords.Size = new System.Drawing.Size(538, 145);
            this.textBoxAllWords.TabIndex = 0;
            this.textBoxAllWords.Text = "Hallo\r\nWTF\r\nInformatik\r\nHowIMetYourMother\r\nComputer\r\nMusik\r\nSchule\r\nHTL\r\nKunst\r\nE" +
    "lektronik";
            // 
            // tabPageAbout
            // 
            this.tabPageAbout.BackColor = System.Drawing.SystemColors.Control;
            this.tabPageAbout.Controls.Add(this.textBoxAbout);
            this.tabPageAbout.Location = new System.Drawing.Point(4, 22);
            this.tabPageAbout.Name = "tabPageAbout";
            this.tabPageAbout.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageAbout.Size = new System.Drawing.Size(563, 366);
            this.tabPageAbout.TabIndex = 2;
            this.tabPageAbout.Text = "About";
            // 
            // textBoxAbout
            // 
            this.textBoxAbout.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxAbout.Enabled = false;
            this.textBoxAbout.Font = new System.Drawing.Font("MS Reference Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAbout.Location = new System.Drawing.Point(7, 7);
            this.textBoxAbout.Multiline = true;
            this.textBoxAbout.Name = "textBoxAbout";
            this.textBoxAbout.Size = new System.Drawing.Size(550, 353);
            this.textBoxAbout.TabIndex = 0;
            this.textBoxAbout.Text = "Hangman by Matthias Lexer\r\n\r\nMan kann sowohl mit Hilfe der Maus als auch mit der\r" +
    "\nTastatur spielen.\r\n\r\nViel Spaß ! :D\r\n";
            this.textBoxAbout.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelWorldSolve
            // 
            this.labelWorldSolve.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelWorldSolve.BackColor = System.Drawing.Color.Cyan;
            this.labelWorldSolve.Location = new System.Drawing.Point(12, 405);
            this.labelWorldSolve.Name = "labelWorldSolve";
            this.labelWorldSolve.Size = new System.Drawing.Size(571, 16);
            this.labelWorldSolve.TabIndex = 1;
            this.labelWorldSolve.Text = "Wort auflösen";
            this.labelWorldSolve.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelWorldSolve.Click += new System.EventHandler(this.labelWorldSolve_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(604, 430);
            this.Controls.Add(this.labelWorldSolve);
            this.Controls.Add(this.tabControlMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.Text = "Hangman";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.FormMain_Paint);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FormMain_KeyPress);
            this.tabControlMain.ResumeLayout(false);
            this.tabPageGame.ResumeLayout(false);
            this.tabPageGame.PerformLayout();
            this.groupBoxStatus.ResumeLayout(false);
            this.groupBoxStatus.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxActualColorSetting)).EndInit();
            this.groupBoxLetters.ResumeLayout(false);
            this.groupBoxLetters.PerformLayout();
            this.groupBoxWord.ResumeLayout(false);
            this.groupBoxControls.ResumeLayout(false);
            this.groupBoxHang.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxImage)).EndInit();
            this.tabPageSettings.ResumeLayout(false);
            this.groupBoxName.ResumeLayout(false);
            this.groupBoxName.PerformLayout();
            this.groupBoxColor.ResumeLayout(false);
            this.groupBoxColor.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxActualColor)).EndInit();
            this.groupBoxWords.ResumeLayout(false);
            this.groupBoxWords.PerformLayout();
            this.tabPageAbout.ResumeLayout(false);
            this.tabPageAbout.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlMain;
        private System.Windows.Forms.TabPage tabPageGame;
        private System.Windows.Forms.GroupBox groupBoxHang;
        private System.Windows.Forms.TabPage tabPageSettings;
        private System.Windows.Forms.GroupBox groupBoxControls;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Button buttonNewGame;
        private System.Windows.Forms.GroupBox groupBoxLetters;
        private System.Windows.Forms.GroupBox groupBoxWord;
        private System.Windows.Forms.GroupBox groupBoxStatus;
        private System.Windows.Forms.Label labelNameSetting;
        private System.Windows.Forms.Label labelColor;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.GroupBox groupBoxName;
        private System.Windows.Forms.GroupBox groupBoxColor;
        private System.Windows.Forms.GroupBox groupBoxWords;
        private System.Windows.Forms.Button buttonNameSave;
        private System.Windows.Forms.TextBox textBoxNameChange;
        private System.Windows.Forms.PictureBox pictureBoxActualColor;
        private System.Windows.Forms.Label labelActualColor;
        private System.Windows.Forms.TextBox textBoxAllWords;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.PictureBox pictureBoxActualColorSetting;
        private System.Windows.Forms.Label labelWord;
        private System.Windows.Forms.Label labelActualWord;
        private System.Windows.Forms.Label labelZ;
        private System.Windows.Forms.Label labelY;
        private System.Windows.Forms.Label labelX;
        private System.Windows.Forms.Label labelW;
        private System.Windows.Forms.Label labelV;
        private System.Windows.Forms.Label labelU;
        private System.Windows.Forms.Label labelT;
        private System.Windows.Forms.Label labelS;
        private System.Windows.Forms.Label labelR;
        private System.Windows.Forms.Label labelQ;
        private System.Windows.Forms.Label labelP;
        private System.Windows.Forms.Label labelO;
        private System.Windows.Forms.Label labelN;
        private System.Windows.Forms.Label labelM;
        private System.Windows.Forms.Label labelL;
        private System.Windows.Forms.Label labelK;
        private System.Windows.Forms.Label labelJ;
        private System.Windows.Forms.Label labelI;
        private System.Windows.Forms.Label labelH;
        private System.Windows.Forms.Label labelG;
        private System.Windows.Forms.Label labelF;
        private System.Windows.Forms.Label labelE;
        private System.Windows.Forms.Label labelD;
        private System.Windows.Forms.Label labelC;
        private System.Windows.Forms.Label labelB;
        private System.Windows.Forms.Label labelA;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.PictureBox pictureBoxImage;
        private System.Windows.Forms.Label labelWorldSolve;
        private System.Windows.Forms.TabPage tabPageAbout;
        private System.Windows.Forms.TextBox textBoxAbout;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

