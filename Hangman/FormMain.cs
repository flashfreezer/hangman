﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;



//Fazit:
//
//TabControls sind scheiße      -       Fenster sind besser - viel besser ;)
//
//Tastatureingabe angefangen (war so ne Last Minute Aktion hab irgendwo nen Fehler drin)
//
//


namespace Hangman
{
    public partial class FormMain : Form
    {
        int iStatus;
        int iIndex;                     //für for - schleifen
        int iWordLength;
        int iActualPosition;
        string[] strWordList;
        string strWordLabelActual;
        string strActualWord;
        string strActualWordLow;
        Random rndNumber;
        int iListLength;
        int iRandomLineLength;
        Label[] oLabelALL;
        Label labelKeyPress;
        //string lkp;  //Abkürzung für labelKeyPress
        System.Drawing.Graphics graphicsObj;

        public FormMain()
        {
            InitializeComponent();

            labelKeyPress = new Label();
            iStatus = 0;
            rndNumber = new Random();
            groupBoxHang.Text = "HangMan";
            textBoxNameChange.Text = "Player";
            strActualWord = "";
            #region olabelALL
            oLabelALL = new Label[27];


            oLabelALL[1] = labelA;
            oLabelALL[2] = labelB;
            oLabelALL[3] = labelC;
            oLabelALL[4] = labelD;
            oLabelALL[5] = labelE;
            oLabelALL[6] = labelF;
            oLabelALL[7] = labelG;
            oLabelALL[8] = labelH;
            oLabelALL[9] = labelI;
            oLabelALL[10] = labelJ;
            oLabelALL[11] = labelK;
            oLabelALL[12] = labelL;
            oLabelALL[13] = labelM;
            oLabelALL[14] = labelN;
            oLabelALL[15] = labelO;
            oLabelALL[16] = labelP;
            oLabelALL[17] = labelQ;
            oLabelALL[18] = labelR;
            oLabelALL[19] = labelS;
            oLabelALL[20] = labelT;
            oLabelALL[21] = labelU;
            oLabelALL[22] = labelV;
            oLabelALL[23] = labelW;
            oLabelALL[24] = labelX;
            oLabelALL[25] = labelY;
            oLabelALL[26] = labelZ;
            #endregion
            graphicsObj = this.pictureBoxImage.CreateGraphics();
            NewGame();
            textBoxAllWords.Enabled = true;

        }
        void PerformLetterClick(Label labelActual)
        {
            strActualWordLow = strActualWord.ToLower();
            textBoxAllWords.Enabled = false;

            if (labelActual.Enabled == true)
            {
                if (strActualWordLow.IndexOf(labelActual.Text) >= 0)
                {
                    labelActual.BackColor = Color.YellowGreen;
                    labelActual.Enabled = false;

                    iActualPosition = strActualWordLow.IndexOf(labelActual.Text);

                    //---------------------------------------Sternchen - Austausch------------------------------//
                    strWordLabelActual = strWordLabelActual.Insert(iActualPosition + 1, labelActual.Text);
                    strWordLabelActual = strWordLabelActual.Remove(iActualPosition, 1);

                    do
                    {
                        iActualPosition = strActualWordLow.IndexOf(labelActual.Text);
                        strActualWordLow = strActualWordLow.Insert(iActualPosition + 1, "*");
                        strActualWordLow = strActualWordLow.Remove(iActualPosition, 1);
                        strWordLabelActual = strWordLabelActual.Insert(iActualPosition + 1, labelActual.Text.ToUpper());
                        strWordLabelActual = strWordLabelActual.Remove(iActualPosition, 1);
                        labelWord.Text = strWordLabelActual;
                    }
                    while (strActualWordLow.IndexOf(labelActual.Text) >= 0);



                    //--------------------------------------------Ende-----------------------------------------//
                }
                else
                {
                    iStatus = iStatus + 1;
                    draw(iStatus);
                    labelActual.Enabled = false;
                    labelActual.BackColor = Color.Red;

                    if (iStatus == 0)
                    {
                        progressBar.Style = ProgressBarStyle.Continuous;
                        progressBar.Value = 6;
                    }
                    else
                    {
                        progressBar.Value = progressBar.Value - 1;
                    }
                }

                if (iStatus == 6)
                {
                    MessageBox.Show("You lose");
                    labelWord.Text = "You Lose , the word was " + strActualWord;
                    labelWord.BackColor = Color.Red;
                    DeactivateAllLabels();
                    labelWorldSolve.Enabled = false;
                }

                else
                {
                    if (labelWord.Text.Contains("*") == false)
                    {
                        MessageBox.Show("You win!");
                        labelWord.Text = " You win , the word was " + strActualWord;
                        labelWord.BackColor = Color.Green;
                        DeactivateAllLabels();
                        labelWorldSolve.Enabled = false;
                    }
                }
            }
            else
            {

                if ((labelActual.Enabled == false) && (labelActual.BackColor != SystemColors.Control))
                {
                    MessageBox.Show("Buchstabe bereits verwendet");
                }
                else
                {
                    MessageBox.Show("Please Press New Game");
                }
            }


        }
        void draw(int iStatus)
        {

            Pen myPen = new Pen(System.Drawing.Color.Green, 3);
            Pen myPenNotGood = new Pen(pictureBoxActualColorSetting.BackColor, 3);
            //myPen.DashStyle = System.Drawing.Drawing2D.DashStyle.DashDotDot;
            //myPen.Color = System.Drawing.Color.RoyalBlue;

            //-------------------------------------------------------------------------------//
            //Standart Zeichnung des Galgens
            graphicsObj.DrawLine(myPen, 35, 200, 175, 200);      //unterste Linie
            graphicsObj.DrawLine(myPen, 100, 200, 100, 50);     //senkrechte Linie nach oben
            graphicsObj.DrawLine(myPen, 100, 50, 200, 50);
            graphicsObj.DrawLine(myPen, 200, 50, 200, 75);
            //------------------------------------------------------------------------------//


            //------------------------------Je nach iStatus Zeichnung--------------------- //

            //einzelne Schritte: umso höher iStatus umso mehr wird gezeichnet
            if (iStatus > 0)
            {
                graphicsObj.DrawEllipse(myPenNotGood, 183, 75, 35, 35);
                // bei iStatus = 1
            }

            if (iStatus > 1)
            {
                graphicsObj.DrawLine(myPenNotGood, 200, 110, 200, 140);
                // bei iStatus = 2
            }

            if (iStatus > 2)
            {
                graphicsObj.DrawLine(myPenNotGood, 200, 140, 220, 160);
                // bei iStatus = 3
            }

            if (iStatus > 3)
            {
                graphicsObj.DrawLine(myPenNotGood, 200, 140, 180, 160);
                // bei iStatus = 4
            }

            if (iStatus > 4)
            {
                graphicsObj.DrawLine(myPenNotGood, 200, 130, 180, 110);
                // bei iStatus = 5
            }

            if (iStatus > 5)
            {
                graphicsObj.DrawLine(myPenNotGood, 200, 130, 220, 110);
                //bei iStatus = 6
            }
            //---------------------------------ENDE-----------------------------------------//
        }
        void DeactivateAllLabels()
        {
            for (int i = 1; i < 27; i++)
            {
                oLabelALL[i].Enabled = false;
            }
        }
        void NewGame()
        {
            labelWorldSolve.Enabled = true;
            textBoxAllWords.Enabled = true;
            //--------------------------------------------Word aussuchen-----------------------------------------------------//
            if (textBoxAllWords.Text == "")
            {
                MessageBox.Show("Die Textbox ist leer, bitte Wörter hinzufügen");
            }
            else
            {
                do
                {
                    strWordList = textBoxAllWords.Lines;
                    iRandomLineLength = strWordList.Length;
                    iListLength = rndNumber.Next(0, iRandomLineLength);
                    strActualWord = strWordList[iListLength];
                }

                while (strActualWord == "" || strActualWord.Length > 20);
            }
                

            for (iIndex = 0; iIndex < 10; iIndex++)     //Kontrollieren ob im ausgewählten String Zahlen vorkommen
            {
                if (strActualWord.Contains(iIndex.ToString()) == true)
                {
                    MessageBox.Show("Bitte schauen sie nach ob in der Wörterliste eine Zahl vorkommt falls ja bitte löschen sie die Zahl heraus");
                    DeactivateAllLabels();
                }
            }
            //-------------------------------------------Ende----------------------------------------------------------------//

            iWordLength = strActualWord.Length;
            strWordLabelActual = "";
            for (iIndex = 1; iIndex <= iWordLength; iIndex++)
            {
                strWordLabelActual = strWordLabelActual + "*";
            }

            labelWord.Text = strWordLabelActual;
            for (iIndex = 1; iIndex < 27; iIndex++)
            {
                oLabelALL[iIndex].Enabled = true;
                oLabelALL[iIndex].BackColor = SystemColors.Control;
            }
            iStatus = 0;

            graphicsObj.Clear(SystemColors.Control);
            draw(iStatus);
            progressBar.Value = 6;
            labelWord.BackColor = SystemColors.Control;
        }

        #region buttonclicks

        private void buttonNewGame_Click(object sender, EventArgs e)
        {
            NewGame();
        }



        private void buttonNameSave_Click(object sender, EventArgs e)
        {
            if (textBoxNameChange.Text.Length > 8)
            {
                textBoxNameChange.Text = "Please take less than 6 letters";
            }
            else
            {
                labelNameSetting.Text = textBoxNameChange.Text;
            }
        }

        private void pictureBoxActualColor_Click(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                if (colorDialog.Color == SystemColors.Control)
                {
                    pictureBoxActualColor.Text = "Choose another Color";
                }
                else
                {
                    pictureBoxActualColor.BackColor = colorDialog.Color;
                    pictureBoxActualColorSetting.BackColor = pictureBoxActualColor.BackColor;
                }
            }
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        #endregion

        #region labelClick

        private void labelA_Click(object sender, EventArgs e)
        {
            PerformLetterClick(labelA);
        }

        private void labelB_Click(object sender, EventArgs e)
        {
            PerformLetterClick(labelB);
        }

        private void labelC_Click(object sender, EventArgs e)
        {
            PerformLetterClick(labelC);
        }

        private void labelD_Click(object sender, EventArgs e)
        {
            PerformLetterClick(labelD);
        }

        private void labelE_Click(object sender, EventArgs e)
        {
            PerformLetterClick(labelE);
        }

        private void labelF_Click(object sender, EventArgs e)
        {
            PerformLetterClick(labelF);
        }

        private void labelG_Click(object sender, EventArgs e)
        {
            PerformLetterClick(labelG);
        }

        private void labelH_Click(object sender, EventArgs e)
        {
            PerformLetterClick(labelH);
        }

        private void labelI_Click(object sender, EventArgs e)
        {
            PerformLetterClick(labelI);
        }

        private void labelJ_Click(object sender, EventArgs e)
        {
            PerformLetterClick(labelJ);
        }

        private void labelK_Click(object sender, EventArgs e)
        {
            PerformLetterClick(labelK);
        }

        private void labelL_Click(object sender, EventArgs e)
        {
            PerformLetterClick(labelL);
        }

        private void labelM_Click(object sender, EventArgs e)
        {
            PerformLetterClick(labelM);
        }

        private void labelN_Click(object sender, EventArgs e)
        {
            PerformLetterClick(labelN);
        }

        private void labelO_Click(object sender, EventArgs e)
        {
            PerformLetterClick(labelO);
        }

        private void labelP_Click(object sender, EventArgs e)
        {
            PerformLetterClick(labelP);
        }

        private void labelQ_Click(object sender, EventArgs e)
        {
            PerformLetterClick(labelQ);
        }

        private void labelR_Click(object sender, EventArgs e)
        {
            PerformLetterClick(labelR);
        }

        private void labelS_Click(object sender, EventArgs e)
        {
            PerformLetterClick(labelS);
        }

        private void labelT_Click(object sender, EventArgs e)
        {
            PerformLetterClick(labelT);
        }

        private void labelU_Click(object sender, EventArgs e)
        {
            PerformLetterClick(labelU);
        }

        private void labelV_Click(object sender, EventArgs e)
        {
            PerformLetterClick(labelV);
        }

        private void labelW_Click(object sender, EventArgs e)
        {
            PerformLetterClick(labelW);
        }

        private void labelX_Click(object sender, EventArgs e)
        {
            PerformLetterClick(labelX);
        }

        private void labelY_Click(object sender, EventArgs e)
        {
            PerformLetterClick(labelY);
        }

        private void labelZ_Click(object sender, EventArgs e)
        {
            PerformLetterClick(labelZ);
        }

        #endregion

        private void FormMain_Paint(object sender, PaintEventArgs e)
        {
            draw(iStatus);
        }
        private void labelWorldSolve_Click(object sender, EventArgs e)
        {
            labelWord.Text = strActualWord;
            DeactivateAllLabels();
            MessageBox.Show("You lose");
        }
        private void FormMain_KeyPress(object sender, KeyPressEventArgs e)
        {
            /*
            lkp = e.KeyChar.ToString();
                if
                    (
                        (lkp != "a") || (lkp != "b") || (lkp != "c") || (lkp != "d") ||
                        (lkp != "e") || (lkp != "f") || (lkp != "g") || (lkp != "h") ||
                        (lkp != "i") || (lkp != "j") || (lkp != "k") || (lkp != "l") ||
                        (lkp != "m") || (lkp != "n") || (lkp != "o") || (lkp != "p") ||
                        (lkp != "q") || (lkp != "r") || (lkp != "s") || (lkp != "t") ||
                        (lkp != "u") || (lkp != "v") || (lkp != "w") || (lkp != "x") ||
                        (lkp != "y") || (lkp != "z") || (lkp != "A") || (lkp != "B") ||
                        (lkp != "C") || (lkp != "D") || (lkp != "E") || (lkp != "F") ||
                        (lkp != "G") || (lkp != "H") || (lkp != "I") || (lkp != "J") ||
                        (lkp != "K") || (lkp != "L") || (lkp != "M") || (lkp != "N") ||
                        (lkp != "O") || (lkp != "P") || (lkp != "Q") || (lkp != "R") ||
                        (lkp != "S") || (lkp != "T") || (lkp != "U") || (lkp != "V") ||
                        (lkp != "W") || (lkp != "X") || (lkp != "Y") || (lkp != "Z")
                    )
                {
                    MessageBox.Show("Error ! - Falsche Eingabe");
                }
       
            //---------------------------------------------------------Key Press Auswertung für obere Leiste--------------------------------------//

                    if (labelKeyPress.Text == "a") { labelKeyPress = labelA; }
                    if (labelKeyPress.Text == "b") { labelKeyPress = labelB; }
                    if (labelKeyPress.Text == "c") { labelKeyPress = labelC; }
                    if (labelKeyPress.Text == "d") { labelKeyPress = labelD; }
                    if (labelKeyPress.Text == "e") { labelKeyPress = labelE; }
                    if (labelKeyPress.Text == "f") { labelKeyPress = labelF; }
                    if (labelKeyPress.Text == "g") { labelKeyPress = labelG; }
                    if (labelKeyPress.Text == "h") { labelKeyPress = labelH; }
                    if (labelKeyPress.Text == "i") { labelKeyPress = labelI; }
                    if (labelKeyPress.Text == "j") { labelKeyPress = labelJ; }
                    if (labelKeyPress.Text == "k") { labelKeyPress = labelK; }
                    if (labelKeyPress.Text == "l") { labelKeyPress = labelL; }
                    if (labelKeyPress.Text == "m") { labelKeyPress = labelM; }
                    if (labelKeyPress.Text == "n") { labelKeyPress = labelN; }
                    if (labelKeyPress.Text == "o") { labelKeyPress = labelO; }
                    if (labelKeyPress.Text == "p") { labelKeyPress = labelP; }
                    if (labelKeyPress.Text == "q") { labelKeyPress = labelQ; }
                    if (labelKeyPress.Text == "r") { labelKeyPress = labelR; }
                    if (labelKeyPress.Text == "s") { labelKeyPress = labelS; }
                    if (labelKeyPress.Text == "t") { labelKeyPress = labelT; }
                    if (labelKeyPress.Text == "u") { labelKeyPress = labelU; }
                    if (labelKeyPress.Text == "v") { labelKeyPress = labelV; }
                    if (labelKeyPress.Text == "w") { labelKeyPress = labelW; }
                    if (labelKeyPress.Text == "x") { labelKeyPress = labelX; }
                    if (labelKeyPress.Text == "y") { labelKeyPress = labelY; }
                    if (labelKeyPress.Text == "z") { labelKeyPress = labelZ; }
                    //------------------------------------------------------------------------Ende----------------------------------------------------------//
                    PerformLetterClick(labelKeyPress);
                
*/
        }
        private void tabControlMain_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*
            if ((tabControlMain.SelectedTab == tabPageSettings) || (tabControlMain.SelectedTab == tabPageSettings))
            {
                if (iStatus != 0)
                {
                    MessageBox.Show("Um irgendetwas zu ändern bitte klicken sie auf NewGame");
                    tabControlMain.SelectedTab = tabPageGame;
                }
            }
            if (tabControlMain.SelectedTab == tabPageGame)
            {
                NewGame();
            }
            */
        }

    }
}
